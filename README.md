### About

A compilation of document templates used by the [VLBA, UOL](https://uol.de/en/computingscience/vlba).

### Downloads

* [thesis-VLBA.zip](latex/thesis-VLBA.zip)
* [latex-beamer-VLBA.zip](latex-beamer/latex-beamer-VLBA.zip)

### Templates

* [latex](latex) - this folder contains LaTeX template, which are used to write bachelor and master theses
* [latex-beamer](latex-beamer) - contains a latex template, which is used to create presentations
* [pptx](https://uol.de/vlba/lehre/hinweise-und-vorlagen) - contains pptx template, which is used to create presentations

### Links

* Further documents could be found [Hinweise und Vorlagen](https://uol.de/vlba/lehre/hinweise-und-vorlagen)
* LaTeX Tool - https://overleaf.com/
